package com.handler;


import com.model.Doctor;
import com.model.DoctorPatientMap;
import com.model.Patient;
import com.repository.DoctorRepo;
import com.repository.PatientDoctorMapRepo;
import com.repository.PatientRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class StorageHandler implements DoctorRepo, PatientRepo, PatientDoctorMapRepo {


    public List<Patient> addALlPatients1() {
        List<Patient> patientList1 = new ArrayList<>();


        patientList1.add(new Patient(1,"Smith"));
        patientList1.add(new Patient(2,"John"));
        patientList1.add(new Patient(103,"Roman"));

        return patientList1;
    }

    public List<Patient> addALlPatients2() {
        List<Patient> patientList2 = new ArrayList<>();


        patientList2.add(new Patient(10,"Sena"));
        patientList2.add(new Patient(5,"Dravid"));
        patientList2.add(new Patient(101,"Rohit"));

        return patientList2;

    }
    public List<Patient> addALlPatients3() {
        List<Patient> patientList3 = new ArrayList<>();

        patientList3.add(new Patient(102,"Rahane"));
        patientList3.add(new Patient(8,"Ganguli"));

        return patientList3;

    }

    @Override
    public Supplier<List<Doctor>> addALlDoctors() {
        List<Doctor> doctorList = new ArrayList<>();
        List<String> slot1=new ArrayList<String>();
        slot1.add("15/09/2022 - [10 AM - 12 PM, 02PM - 05PM]");
        slot1.add("17/09/2022 - [09 AM - 11 AM, 01PM - 04PM]");
        List<String> slot2=new ArrayList<String>();
        slot2.add("16/09/2022 - [09AM - 11PM, 04 PM - 07PM");
        slot2.add("19/09/2022 - [10AM - 12PM, 05 PM - 08PM");
        return () -> {
            doctorList.add(new Doctor(101, "Phani", "General Surgeon", "Available", "14/09/202",addALlPatients1(),slot1));
            doctorList.add(new Doctor(102,"Neethu","Dermatologist","Unavailable","12/09/2022",addALlPatients2(),slot2));
            doctorList.add(new Doctor(103,"Kumar","Orthopaedics","Available","15/09/2022",addALlPatients3(),slot1));

            return doctorList;

        };
    }

    @Override
    public Supplier<List<Patient>> addALlpatients() {
        List<Patient> patientList1 = new ArrayList<>();

        return () -> {
            patientList1.add(new Patient(1,"Smith"));
            patientList1.add(new Patient(2,"John"));
            patientList1.add(new Patient(103,"Roman"));
            patientList1.add(new Patient(10,"Sena"));
            patientList1.add(new Patient(5,"Dravid"));
            patientList1.add(new Patient(101,"Rohit"));
            patientList1.add(new Patient(102,"Rahane"));
            patientList1.add(new Patient(8,"Ganguli"));

            return patientList1;
        };
    }

    @Override
    public Supplier<List<DoctorPatientMap>> getDoctorPatientMap() {
        List<DoctorPatientMap> doctorPatientMaps = new ArrayList<>();
        return () -> {
            doctorPatientMaps.add(new DoctorPatientMap(101,1));
            doctorPatientMaps.add(new DoctorPatientMap(102,2));
            doctorPatientMaps.add(new DoctorPatientMap(103,103));
            doctorPatientMaps.add(new DoctorPatientMap(102,10));
            doctorPatientMaps.add(new DoctorPatientMap(103,5));


            return doctorPatientMaps;
        };
    }


}
