package com.handler;

import com.model.Doctor;
import com.repository.DoctorRepo;
import com.service.DoctorService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DoctorHandler implements DoctorService {

    DoctorRepo storage= new StorageHandler();

    @Override
    public Map<String, Integer> addPatientsSizeForDoctor() {
        return storage.addALlDoctors().get().stream().collect(Collectors.toMap(m -> m.getName(), m -> m.getDoctorPatientList().size()));
    }

    @Override
    public void getDoctorSlots(String name) {
        List<Doctor> doctorslots = storage.addALlDoctors().get().stream().filter(m -> m.getName() == name).filter(s->s.getName()==name).toList();
        System.out.print("Dr. "+name+" available slots are : ");
        System.out.println(doctorslots.stream().map(Doctor::getSlots).collect(Collectors.toList()));

    }
}
