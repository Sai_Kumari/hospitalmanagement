package com.repository;

import com.model.Doctor;

import java.util.List;
import java.util.function.Supplier;

public interface DoctorRepo {
    public Supplier<List<Doctor>> addALlDoctors();

}
