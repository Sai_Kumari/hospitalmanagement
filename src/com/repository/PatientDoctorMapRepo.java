package com.repository;

import com.model.DoctorPatientMap;

import java.util.List;
import java.util.function.Supplier;

public interface PatientDoctorMapRepo {
    public Supplier<List<DoctorPatientMap>> getDoctorPatientMap();
}
