package com.model;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class Doctor {
    private int id;

    private String name;

    private String specialization;

   private String status;

   private String appointmentDate;

    List<Patient> doctorPatientList = new ArrayList<Patient>();

    private List<String> slots;

    public Doctor(int id, String name, String specialization, String status, String appointmentDate, List<Patient> doctorPatientList, List<String> slots) {
        this.id = id;
        this.name = name;
        this.specialization = specialization;
        this.status = status;
        this.appointmentDate = appointmentDate;
        this.doctorPatientList = doctorPatientList;
        this.slots = slots;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public List<Patient> getDoctorPatientList() {
        return doctorPatientList;
    }

    public void setDoctorPatientList(List<Patient> doctorPatientList) {
        this.doctorPatientList = doctorPatientList;
    }

    public List<String> getSlots() {
        return slots;
    }

    public void setSlots(List<String> slots) {
        this.slots = slots;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", specialization='" + specialization + '\'' +
                ", status='" + status + '\'' +
                ", appointmentDate='" + appointmentDate + '\'' +
                '}';
    }
}
