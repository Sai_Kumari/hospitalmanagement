package com.model;

public class Patient {
    private Integer pId;
    private String name;

    public Patient(Integer pId, String name) {
        this.pId = pId;
        this.name = name;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "pId=" + pId +
                ", name='" + name + '\'' +
                '}';
    }
}
