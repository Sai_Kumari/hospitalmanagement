package com.model;

public class DoctorPatientMap {


        private int doctor_id;
        private int patient_id;

        public int getDoctor_id() {
            return doctor_id;
        }

        public int getPatient_id() {
            return patient_id;
        }

        public void setDoctor_id(int doctor_id) {
            this.doctor_id = doctor_id;
        }

        public void setPatient_id(int patient_id) {
            this.patient_id = patient_id;
        }

        public DoctorPatientMap(int doctor_id, int patient_id) {
            this.doctor_id = doctor_id;
            this.patient_id = patient_id;
        }

        @Override
        public String toString() {
            return "DoctorPatientMap{" +
                    "doctor_id=" + doctor_id +
                    ", patient_id=" + patient_id +"\n";
        }


}
